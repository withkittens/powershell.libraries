﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerShell.Libraries
{
    public static class PathHelper
    {
        public static bool IsLibrariesFolder(string path)
        {
            return path.Equals(@"Libraries:\", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsLibraryRoot(string path)
        {
            var driveSeparatorIndex = path.IndexOf(":\\");
            var driveLessPath = path.Substring(driveSeparatorIndex + 2);
            return driveLessPath.IndexOf('\\') == -1;
        }

        public static string GetLibraryName(string path)
        {
            var driveSeparatorIndex = path.IndexOf(":\\");
            var driveLessPath = path.Substring(driveSeparatorIndex + 2);
            var backslashIndex = driveLessPath.IndexOf('\\');
            var length = backslashIndex != -1 ? backslashIndex : driveLessPath.Length;
            return driveLessPath.Substring(0, length);
        }
    }
}
