﻿#define LOCATIONLESS_LIBRARY_IS_CONTAINER

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Provider;
using System.Text;
using System.Threading.Tasks;

namespace PowerShell.Libraries
{
    [CmdletProvider("Libraries", ProviderCapabilities.None)]
    public class LibrariesProvider : NavigationCmdletProvider
    {
        protected override bool IsValidPath(string path)
        {
            WriteVerbose("LibrariesProvider::IsValidPath(path='{0}')", path);
            throw new NotImplementedException();
        }

        protected override string[] ExpandPath(string path)
        {
            return base.ExpandPath(path);
        }

        protected override bool ItemExists(string path)
        {
            WriteVerbose("LibrariesProvider::ItemExists(path='{0}')", path);
            return true;
        }

        protected override bool IsItemContainer(string path)
        {
            WriteVerbose("LibrariesProvider::IsItemContainer");
            WriteVerbose("Path: '{0}'", path);

            if (PathHelper.IsLibrariesFolder(path))
            {
                return true;
            }
            else if (PathHelper.IsLibraryRoot(path))
            {
                //проверить, есть ли у библиотеки локейшены
                WriteError(new ErrorRecord(new InvalidOperationException(), "InvalidOperationException", ErrorCategory.InvalidOperation, path));
                return false;
            }
            else
            {
                //мы внутри какой-то библиотеки, например:
                //cd Libraries:\Documents\Visual Studio 2012
                //1. Получить default save location библиотеки
                //2. Проверить наличие айтема там
                //2.1. Если айтем есть, проверить контейнерность
                //2.2. Если нету
                //2.2.1. Получить локейшены библиотеки
                //2.2.2. В каждом локейшене проверить наличие айтема
                //2.2.2.1. Если есть, проверить контейнерность
                return false;

                //SessionState.InvokeProvider.Item.IsContainer("Filesystem::...");
            }
        }

        

        protected override void GetChildItems(string path, bool recurse)
        {
            WriteVerbose("LibrariesProvider::GetChildItems");
            WriteVerbose("Path: '{0}'", path);
            WriteVerbose("Recurse: {0}", recurse);

            if (PathHelper.IsLibrariesFolder(path))
            {
                foreach (var libraryInfo in LibraryManager.GetLibraries())
                {
                    if (!recurse)
                    {
#if LOCATIONLESS_LIBRARY_IS_CONTAINER
                        WriteItemObject(
                            item: libraryInfo,
                            path: base.MakePath(path, libraryInfo.Name),
                            isContainer: true);
#else
                        WriteItemObject(
                            item: libraryInfo,
                            path: base.MakePath(path, libraryInfo.Name),
                            isContainer: libraryInfo.Locations.Count != 0);
#endif
                    }
                    else
                    {
                        //var ps = SessionState.InvokeProvider.ChildItem.Get(@"FileSystem::C:\Windows", true);
                        //SessionState.Provider.GetOne("f")
                        //var cmdlet = SessionState.InvokeCommand.GetCmdlet("Get-ChildItem");

                        foreach (var location in libraryInfo.Locations)
                        {
                            SessionState.InvokeCommand.InvokeScript(
                                "Get-ChildItem \"" + "Filesystem::" + location.ToString() + "\" -Recurse | Out-Default");
                            /*try
                            {
                                foreach (var fileSystemInfo in location
                                    .EnumerateFileSystemInfos("*", SearchOption.AllDirectories))
                                {
                                    WriteItemObject(
                                        item: fileSystemInfo,
                                        path: "FileSystem::" + fileSystemInfo.FullName,
                                        isContainer: fileSystemInfo.Attributes.HasFlag(FileAttributes.Directory));

                                    
                                }
                            }
                            catch (UnauthorizedAccessException ex)
                            {
                                WriteError(
                                    exception: ex,
                                    errorId: "UnauthorizedFileAccess",
                                    errorCategory: ErrorCategory.PermissionDenied,
                                    targetObject: null);
                            }*/
                        }
                    }
                }
            }
        }

        protected override Collection<PSDriveInfo> InitializeDefaultDrives()
        {
            var driveInfos = new Collection<PSDriveInfo>();
            driveInfos.Add(new PSDriveInfo("Libraries", this.ProviderInfo, @"Libraries:\", string.Empty, null));
            return driveInfos;
        }

        private void WriteVerbose(string format, params object[] args)
        {
            base.WriteVerbose(string.Format(format, args));
        }

        private void WriteError(Exception exception, string errorId, ErrorCategory errorCategory, object targetObject)
        {
            base.WriteError(new ErrorRecord(exception, errorId, errorCategory, targetObject));
        }

        private void ThrowTerminatingError(Exception exception, string errorId, ErrorCategory errorCategory, object targetObject)
        {
            base.ThrowTerminatingError(new ErrorRecord(exception, errorId, errorCategory, targetObject));
        }
    }
}
