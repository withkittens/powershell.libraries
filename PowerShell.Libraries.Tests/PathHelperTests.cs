﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerShell.Libraries;

namespace PowerShell.Libraries.Tests
{
    [TestClass]
    public class PathHelperTests
    {
        [TestMethod]
        public void IsLibrariesFolder_Libraries_ShouldReturnTrue()
        {
            // Libraries:\
            Assert.IsTrue(PathHelper.IsLibrariesFolder(""));
        }

        [TestMethod]
        public void IsLibrariesFolder_LibrariesDocuments_ShouldReturnFalse()
        {
            // Libraries:\Documents
            Assert.IsFalse(PathHelper.IsLibrariesFolder("Documents"));
        }
    }
}
