#include "stdafx.h"
#include "LibraryManager.h"
#include "LibraryInfo.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

namespace PowerShell
{
    namespace Libraries
    {
        IReadOnlyCollection<LibraryInfo^>^ LibraryManager::GetLibraries()
        {
            auto libraries = gcnew List<LibraryInfo^>;

            CComPtr<IShellItem> librariesItem;
            auto hr = SHGetKnownFolderItem(FOLDERID_Libraries, KF_FLAG_DEFAULT, 0, IID_PPV_ARGS(&librariesItem));
            if (SUCCEEDED(hr))
            {
                CComPtr<IEnumShellItems> enumerator;
                hr = librariesItem->BindToHandler(nullptr, BHID_EnumItems, IID_PPV_ARGS(&enumerator));
                if (SUCCEEDED(hr))
                {
                    CComPtr<IShellItem> library;
                    while ((hr = enumerator->Next(1, &library, nullptr)) == S_OK)
                    {
                        CComHeapPtr<WCHAR> parsingName;
                        hr = library->GetDisplayName(SIGDN_DESKTOPABSOLUTEPARSING, &parsingName);
                        if (SUCCEEDED(hr))
                        {
                            auto libraryInfo = gcnew LibraryInfo(gcnew String(parsingName));
                            libraries->Add(libraryInfo);
                        }
                        else
                            break;

                        library.Release();
                    }//enumerator->Next

                    if (SUCCEEDED(hr))
                        return libraries;
                }//librariesItem->BindToHandler
            }//SHGetKnownFolderItem

            Marshal::ThrowExceptionForHR(hr);
            throw gcnew COMException(nullptr, hr);
        }
    }
}