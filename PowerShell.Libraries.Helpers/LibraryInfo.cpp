#include "stdafx.h"
#include "LibraryInfo.h"

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

namespace PowerShell
{
    namespace Libraries
    {
        String^ LibraryInfo::Name::get()
        {
            pin_ptr<const wchar_t> pwszParsingName = PtrToStringChars(_parsingName);

            CComPtr<IShellItem> libraryItem;
            auto hr = SHCreateItemFromParsingName(pwszParsingName, nullptr, IID_PPV_ARGS(&libraryItem));
            if (SUCCEEDED(hr))
            {
                CComHeapPtr<WCHAR> displayName;
                hr = libraryItem->GetDisplayName(SIGDN_NORMALDISPLAY, &displayName);
                if (SUCCEEDED(hr))
                    return gcnew String(displayName);
            }

            if (FAILED(hr))
                Marshal::ThrowExceptionForHR(hr);

            return nullptr;
        }

        IReadOnlyCollection<DirectoryInfo^>^ LibraryInfo::Locations::get()
        {
            auto locations = gcnew List<DirectoryInfo^>;

            pin_ptr<const wchar_t> pwszParsingName = PtrToStringChars(_parsingName);

            CComPtr<IShellLibrary> library;
            auto hr = SHLoadLibraryFromParsingName(pwszParsingName, STGM_READ, IID_PPV_ARGS(&library));
            if (SUCCEEDED(hr))
            {
                CComPtr<IShellItemArray> items;
                auto hr = library->GetFolders(LFF_FORCEFILESYSTEM, IID_PPV_ARGS(&items));
                if (SUCCEEDED(hr))
                {
                    DWORD itemCount;
                    hr = items->GetCount(&itemCount);
                    if (SUCCEEDED(hr))
                    {
                        if (itemCount == 0)
                            return locations;

                        CComPtr<IEnumShellItems> enumerator;
                        hr = items->EnumItems(&enumerator);
                        if (SUCCEEDED(hr))
                        {
                            CComPtr<IShellItem> shellItem;
                            while ((hr = enumerator->Next(1, &shellItem, nullptr)) == S_OK)
                            {
                                CComHeapPtr<WCHAR> ppszName;
                                hr = shellItem->GetDisplayName(SIGDN_FILESYSPATH, &ppszName);
                                if (SUCCEEDED(hr))
                                {
                                    auto directoryInfo = gcnew DirectoryInfo(gcnew String(ppszName));
                                    locations->Add(directoryInfo);
                                }
                                else
                                    break;

                                shellItem.Release();
                            }//enumerator->Next

                            if (SUCCEEDED(hr))
                                return locations;
                        }//items->EnumItems
                    }//items->GetCount
                }//library->GetFolders
            }//SHLoadLibraryFromParsingName

            Marshal::ThrowExceptionForHR(hr);
            throw gcnew COMException(nullptr, hr);
        }
    }
}