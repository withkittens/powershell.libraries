#pragma once

namespace PowerShell
{
    namespace Libraries
    {
        ref class LibraryInfo;

        public ref class LibraryManager sealed
        {
        private:
            LibraryManager() {}

        public:
            static System::Collections::Generic::IReadOnlyCollection<LibraryInfo^>^ GetLibraries();
        };
    }
}