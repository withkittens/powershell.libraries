#pragma once

namespace PowerShell
{
    namespace Libraries
    {
        [System::Diagnostics::DebuggerDisplay("{DebuggerDisplay,nq}")]
        public ref class LibraryInfo sealed
        {
        private:
            System::String^ _parsingName;

        internal:
            LibraryInfo(System::String^ parsingName)
                : _parsingName(parsingName) {}

        public:
            property System::String^ Name { System::String^ get(); }
            property System::Collections::Generic::IReadOnlyCollection<System::IO::DirectoryInfo^>^ Locations
            {
                System::Collections::Generic::IReadOnlyCollection<System::IO::DirectoryInfo^>^ get();
            }

        private:
            property System::String^ DebuggerDisplay
            {
                System::String^ get()
                {
                    return System::String::Format("{{{0}, {1}}}", Name, _parsingName);
                }
            }
        };
    }
}